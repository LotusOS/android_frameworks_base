/*
* Copyright (C) 2017 The Pixel Experience Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.android.internal.util.lotus;

public class OverlayUtils {
    public static String[] AllPackages = {"com.android.documentsui.theme.dark",
                "com.android.systemui.custom.theme.dark",
                "com.android.gboard.theme.dark",
                "com.android.settings.intelligence.theme.dark",
                "com.android.wellbeing.theme",
                "com.android.system.theme.dark",
                "com.android.settings.theme.dark",
                "org.pixelexperience.ota.theme.dark",
                "com.android.documentsui.theme.black",
                "com.android.dialer.theme.black",
                "com.android.contacts.theme.black", 
                "com.android.dialer.theme.dark",
                "com.android.contacts.theme.dark",                           
                "com.android.systemui.custom.theme.black",
                "com.android.system.theme.black",
                "org.pixelexperience.ota.theme.black",
                "org.pixelexperience.overlay.accent.cyan",
                "org.pixelexperience.overlay.accent.purple",
                "org.pixelexperience.overlay.accent.white",
                "org.pixelexperience.overlay.accent.red",
                "org.pixelexperience.overlay.accent.brown",
                "org.pixelexperience.overlay.accent.yellow",
                "org.pixelexperience.overlay.accent.teal",
                "org.pixelexperience.overlay.accent.black",
                "org.pixelexperience.overlay.accent.orange",
                "org.pixelexperience.overlay.accent.green",
                "org.evolutionx.overlay.accent.pixelblue",
                "org.evolutionx.overlay.accent.qcinnamon",
                "org.evolutionx.overlay.accent.qocean",
                "org.evolutionx.overlay.accent.qorchid",
                "org.evolutionx.overlay.accent.qspace",
                "org.pixelexperience.overlay.accent.pink",
                "com.google.android.theme.newhouseorange", 
                "com.google.android.theme.sunsetorange", 
                "com.google.android.theme.warmthorange", 
                "com.google.android.theme.maniamber", 
                "com.google.android.theme.limedgreen", 
                "com.google.android.theme.diffdaygreen",
                "com.google.android.theme.spoofygreen",
                "com.google.android.theme.movemint",
                "com.google.android.theme.naturedgreen", 
                "com.google.android.theme.stock", 
                "com.google.android.theme.drownedaqua", 
                "com.google.android.theme.holillusion", 
                "com.google.android.theme.coldbleu", 
                "com.google.android.theme.heirloombleu", 
                "com.google.android.theme.obfusbleu", 
                "com.google.android.theme.almostproblue",
                "com.google.android.theme.lunablue", 
                "com.google.android.theme.frenchbleu", 
                "com.google.android.theme.dreamypurple", 
                "com.google.android.theme.notimppurple", 
                "com.google.android.theme.grapespurple",
                "com.google.android.theme.spookedpurple", 
                "com.google.android.theme.dimigouig", 
                "com.google.android.theme.duskpurple", 
                "com.google.android.theme.bubblegumpink", 
                "com.google.android.theme.dawnred", 
                "com.google.android.theme.burningred", 
                "com.google.android.theme.labouchered", 
                "com.google.android.theme.misleadingred", 
                "com.google.android.theme.whythisgrey", 
                "com.android.gboard.theme.light",
                "org.pixelexperience.overlay.hidecutout",
                "com.android.facelock.theme.dark",
                "com.google.android.setupwizard.overlay"};
}